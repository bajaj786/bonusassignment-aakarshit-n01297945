﻿<%@ Page Title="Cartesian" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Cartesian.aspx.cs" Inherits="BonusAssignment_1_.Cartesian" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Data1" runat="server"><br/>
     <div>
        <asp:Label runat="server" Font-Size="Large">Value For X-Axis Please!</asp:Label><br/><br/>
        
         <asp:TextBox runat="server" ID="XAxis"></asp:TextBox><br/>
        
         <asp:RequiredFieldValidator ID="XAxisReqd" runat="server" ControlToValidate="XAxis" ErrorMessage="I can't Proceed without X-Axis value" Display="Dynamic"></asp:RequiredFieldValidator>
         
         <asp:RegularExpressionValidator ID="XAxisRegExp" runat="server" ControlToValidate="XAxis" ErrorMessage="Enter a valid Numeric Value" ValidationExpression="[\-]?[0-9]{0,7}([\.][0-9])?$"  Display="Dynamic"></asp:RegularExpressionValidator>
         
         <asp:CustomValidator ID="XAxisCustValid" runat="server" ControlToValidate="XAxis" OnServerValidate="ServerValidateXaxis" ErrorMessage="Non-Zero Value for X-Axis Please!"  Display="Dynamic"></asp:CustomValidator>
        
         <br />

        <asp:Label runat="server" Font-Size="Large">Value For Y-Axis Please!</asp:Label><br/><br/>

        <asp:TextBox runat="server" ID="AxisY"></asp:TextBox><br/>
        
        <asp:RequiredFieldValidator ID="AxisYReqd" runat="server" ControlToValidate="AxisY" ErrorMessage="I can't Proceed without Y-Axis value" Display="Dynamic"></asp:RequiredFieldValidator>
        
        <asp:RegularExpressionValidator ID="AxisYRegExp" runat="server" ControlToValidate="AxisY" ErrorMessage="Enter a valid Numeric Value" ValidationExpression="[\-]?[0-9]{0,7}([\.][0-9])?$" Display="Dynamic"></asp:RegularExpressionValidator>
        
        <asp:CustomValidator ID="AxisYCustValid" runat="server" ControlToValidate="AxisY" OnServerValidate="ServerValidateYaxis" ErrorMessage="Non-Zero Value for Y-Axis Please!" Display="Dynamic"></asp:CustomValidator>
        
        <br />
        <br />
        <asp:Button ID="SubmitInput" Text="Check Quadrant" runat="server" OnClick="Quadrant_checker"/>
        <div id="OutputValue" runat="server"></div>
    </div>
</asp:Content>
