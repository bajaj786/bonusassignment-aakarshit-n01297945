﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssignment_1_
{
    public partial class PrimeNumber : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void PrimeNumberChecker(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
                return;
            }

            int NInput = int.Parse(NumberInput1.Text);

            bool flag = false;

            for (int i = NInput - 1; i > 1; i--)
            {
                if (NInput % i == 0)
                {
                    flag = true;
                    break;
                }
            }

            if (flag == false)
            {
                OutputString.InnerHtml = "<strong> Result:</strong>  The Number " + NInput + " is Prime";
            }
            else
            {
                OutputString.InnerHtml = "<strong> Result:</strong>  The Number " + NInput + " is not Prime";
            }
        }
    }
}
