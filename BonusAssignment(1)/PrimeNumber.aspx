﻿<%@ Page Title="PrimeNumber" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PrimeNumber.aspx.cs" Inherits="BonusAssignment_1_.PrimeNumber" %>
       

     

<asp:Content ID="Content2" ContentPlaceHolderID="Data1" runat="server">
    
<div runat="server">
    <br />
    <br />
   <asp:Label runat="server" Font-Size="Large">Enter number to be checked for Prime Number</asp:Label>
          <!-----------------------------TextBoxStarted---------------------------- -->
    <br />
   <asp:TextBox runat="server" ID="NumberInput1"></asp:TextBox>
          <!-----------------------------Validator'sStarted---------------------------- -->
   <asp:RequiredFieldValidator ID="NumberInput1Required" runat="server" ControlToValidate="NumberInput1" ErrorMessage="I can't Proceed Without Number" Display="Dynamic">
   </asp:RequiredFieldValidator>

   <asp:RegularExpressionValidator ID="NumberInput1RegularExpression" runat="server" ControlToValidate="NumberInput1" ErrorMessage="Valid Input Number Please!" ValidationExpression="[0-9]+$" Display="Dynamic">
   </asp:RegularExpressionValidator>
   <br />
   <br />
          <!-----------------------------SubmitButton---------------------------- -->
   <asp:Button ID="submitNumberInput1" Text="Prime Number Checker" runat="server" OnClick="PrimeNumberChecker"/>
</div>

    <br />
    <div id="OutputString" runat="server"></div>
</asp:Content>

