﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssignment_1_
{
    public partial class Cartesian : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void Quadrant_checker(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
                return;
            }

            double xaxisValue = Convert.ToDouble(XAxis.Text);
            double yaxisValue = Convert.ToDouble(AxisY.Text);

            if (xaxisValue > 0 && yaxisValue > 0)
            {
                OutputValue.InnerHtml = "<strong>Result:</strong> (" + XAxis.Text + "," + AxisY.Text + ") lies in 1st Quadrant ";
            }
            else if (xaxisValue < 0 && yaxisValue > 0)
            {
                OutputValue.InnerHtml = "<strong>Result:</strong> (" + XAxis.Text + "," + AxisY.Text + ") lies in 2nd Quadrant ";
            }
            else if (xaxisValue < 0 && yaxisValue < 0)
            {
                OutputValue.InnerHtml = "<strong>Result:</strong> (" + XAxis.Text + "," + AxisY.Text + ") lies in 3rd Quadrant ";
            }
            else if (xaxisValue > 0 && yaxisValue < 0)
            {
                OutputValue.InnerHtml = "<strong>Result:</strong> (" + XAxis.Text + "," + AxisY.Text + ") lies in 4th Quadrant ";
            }
        }

        protected void ServerValidateXaxis(object source, ServerValidateEventArgs args)
        {
            if (Convert.ToDouble(XAxis.Text) == 0)
            {
                args.IsValid = false;
            }
        }
        protected void ServerValidateYaxis(object source, ServerValidateEventArgs args)
        {
            if (Convert.ToDouble(AxisY.Text) == 0)
            {
                args.IsValid = false;
            }
        }

    }
}
    
