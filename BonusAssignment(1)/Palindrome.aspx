﻿<%@ Page Title="Palindrome" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Palindrome.aspx.cs" Inherits="BonusAssignment_1_.Palindrome" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Data1" runat="server">
    <div>
        <br />
        <br />
        <asp:Label runat="server" Font-Size="Large">Enter the String Please!</asp:Label>
        <br />
        <br />
        <asp:TextBox runat="server" ID="PalindromeString"></asp:TextBox>
        
        <asp:RequiredFieldValidator ID="PalindromeValidator1" runat="server" ControlToValidate="PalindromeString" ErrorMessage="I can't proceed without any value" ForeColor="Green" Display="Dynamic">
        </asp:RequiredFieldValidator>
        
        <asp:RegularExpressionValidator ID="PalindromeValidator2" runat="server" ControlToValidate="PalindromeString" ErrorMessage="Enter an appropriate answer" ValidationExpression="[a-zA-Z ]+$" ForeColor="Red" Display="Dynamic"></asp:RegularExpressionValidator>
        <br />
        <br />
        
        <asp:Button ID="PalindromeSubmit" Text="Check Palindrome String" runat="server" OnClick="PalindromeSubmitClick"/>
    </div>
    <br />
    <div id="OutputString" runat="server"></div>
</asp:Content>
