﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssignment_1_
{
    public partial class Palindrome : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void PalindromeSubmitClick(object sender, EventArgs e)
        {
          
            if (!Page.IsValid)
            {
                return;
            }

            string StringInput = PalindromeString.Text;

            string LowerCaseInputString = StringInput.ToLower();
            string StringInputWithoutSpaces = LowerCaseInputString.Replace(" ", string.Empty);
            string ReverseInputString = string.Empty;

            


            int length = StringInputWithoutSpaces.Length;
            bool flag = false;
            for (int i = 0; i < length / 2; i++)
            {
                if (StringInputWithoutSpaces[i] != StringInputWithoutSpaces[length - i - 1])
                {
                    flag = true;
                    break;
                }
            }

            if (flag == false)
            {
                OutputString.InnerHtml = "<strong>OutputString:</strong> Yipee! " + StringInput + " is a Palindrome";
            }
            else
            {
                OutputString.InnerHtml = "<strong>OutputString:</strong> Sorry! " + StringInput + " is not a Palindrome";
            }
        }
    }
}